package prova.controller;

import java.util.ArrayList;

import prova.model.Medico;
import prova.model.Paciente;

public class ControlaMedico {
	
	ArrayList<Medico> listaMedico = new ArrayList<Medico>();
	
	public void adicionar(Medico medico){
		listaMedico.add(medico);
	}
	public void remover(Medico medico){
		listaMedico.remove(medico);
	}

	public void listarMedico(){
		for(Medico medico : listaMedico){
			System.out.println("Nomes: "+medico.getNome());
		}
	}
}
