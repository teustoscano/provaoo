package prova.controller;

import java.util.*;

import prova.model.Consulta;
import prova.model.Paciente;

public class ControlaPaciente {

	ArrayList<Paciente> listaPaciente = new ArrayList<Paciente>();
	
	public void adicionar(Paciente paciente){
		listaPaciente.add(paciente);
	}
	public void remover(Paciente paciente){
		listaPaciente.remove(paciente);
	}
	public void acessarConsulta(Paciente paciente){
		System.out.println("Nome: "+paciente.getNome());
		System.out.println("Tipo de consulta: "+paciente.getEspecialidadeMedica());
		System.out.println("Nome Medico: "+paciente.consulta.getMedico().getNome());
		System.out.println("Orientação: "+paciente.consulta.getOrientacao());
		System.out.println("Medicação Preescrita: "+paciente.consulta.getMedicacao());
	}
	public void listarpaciente(){
		for(Paciente paciente : listaPaciente){
			System.out.println("Nome: "+paciente.getNome());
		}
}
	}
